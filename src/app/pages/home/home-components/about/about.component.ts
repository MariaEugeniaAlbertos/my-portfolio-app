import { IAbout } from './../../models/IHome';
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  public resume: string;
  public titleAbout: string;
  public subtitle: string;
  public infoAbout: string;
  @Input() public dataHomeAbout: IAbout;
  pageYoffset = 0;
  @HostListener('window:scroll', ['$event']) onScroll(event){
    this.pageYoffset = window.pageYOffset;
  }

    constructor(private scroll: ViewportScroller) { }


  ngOnInit(): void {
    this.resume = 'Resume';
    this.titleAbout = 'About';
    this.subtitle = "HI! I'M MARIU AND I'M FRONT-END DEVELOPER."
    this.infoAbout = "Communication is my passion, and this is the reason why I decided 10 years ago to study and specialise in this field. We are in a world of continuous change and evolution, and the same happens with communication. It is not only about programming and giving solutions to a problem, but how important it is to know how to transmit and communicate it. My desire is to ensure that the customer experience is the best possible and help them not only to achieve their goals but also to communicate them effectively.";
  }
  scrollToTop(){
    this.scroll.scrollToPosition([0, 0]);
  }

 
}

