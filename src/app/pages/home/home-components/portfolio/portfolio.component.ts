import { IPortfolio } from './../../models/IHome';
import { Component, OnInit, Input } from '@angular/core';



@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  public titlePortfolio: string;
  @Input() public dataHomePortfolio: IPortfolio;





  constructor() {


  }



  ngOnInit(): void {

    this.titlePortfolio = "Portfolio";
  }



  // tslint:disable-next-line: typedef
  goToLink(url: string){
    window.open(url, '_blank');
}
}

