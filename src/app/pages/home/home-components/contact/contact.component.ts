import { Component } from '@angular/core';
import { MessageService } from './../../../../services/message.service';
import * as swal from 'sweetalert';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent {
  constructor(public _MessageService: MessageService) {}
  contactForm(form) {
    this._MessageService.sendMessage(form).subscribe(() => {
      swal.default(
        'Contact form',
        'Message sent!',
        'success'
      );
    });
  }
}
