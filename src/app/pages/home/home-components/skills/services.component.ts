import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  public titleSkills: string;
  public titleSkills1: string;
  public titleSkills2: string;
  public titleSkills3: string;
  public titleSkills4: string;
  public titleSkills5: string;
  public titleSkills6: string;

  constructor() {

    this.titleSkills = 'Services'
    this.titleSkills1 = 'HTML';
    this.titleSkills2 = 'CSS';
    this.titleSkills3 = 'JAVASCRIPT';
    this.titleSkills4 = 'MOBILE FIRST';
    this.titleSkills5 = 'ANGULAR';
    this.titleSkills6 = 'REACT';
  }

  ngOnInit(): void {
  }

}

