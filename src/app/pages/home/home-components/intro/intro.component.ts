import { IIntro } from './../../models/IHome';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
  public titleIntro:string;

  @Input() public dataHomeIntro: IIntro;

  constructor() {
    
  }

  ngOnInit(): void {
    this.titleIntro= "Stop trying, I'm here to save you."
  }

}
