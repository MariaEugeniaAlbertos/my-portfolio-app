export interface IHome {
  titleIntro: string;
  titleAbout: string;
  subtitle: string;
  infoAbout: string;
  titlePortfolio: string;
}

export interface IIntro {
  titleIntro: string;
}

export interface IAbout {
  titleAbout: string;
  subtitle: string;
  infoAbout: string;

}


export interface IPortfolio {
  titlePortfolio: string;
}




export interface IContact {
  name: string;
  email: string;
  phone: string;
  message: string;
}


