import { ContactComponent } from './home-components/contact/contact.component';
import {
  IIntro,
  IAbout,
  IHome,
  IPortfolio
} from './models/IHome';
import { HomeService } from './services/home.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public dataHome: IHome;
  public dataHomeIntro: IIntro;
  public dataHomeAbout: IAbout;
  public dataHomePortfolio: IPortfolio;


  constructor(private homeService: HomeService) {}

  ngOnInit(): void {
    this.getHomeData();
  }

  public getHomeData(): void {
    this.homeService.getHome().subscribe(
      (data: IHome) => {
        this.mapHome(data);
        console.log('data:', data);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  public mapHome(data: IHome): void {
    this.dataHomeIntro = {
      titleIntro: data[0].titleIntro,
    };
    this.dataHomeAbout = {
      titleAbout: data[0].titleAbout,
      subtitle: data[0].subtitle,
      infoAbout: data[0].infoAbout,
    };
    this.dataHomePortfolio = {
      titlePortfolio: data[0].titlePortfolio,
    };
  }
}
