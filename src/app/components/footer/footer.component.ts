import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
public rights: string;
  constructor() { 
    this.rights= '© María Eugenia Albertos, 2021'
  }

  ngOnInit(): void {
  }

}
