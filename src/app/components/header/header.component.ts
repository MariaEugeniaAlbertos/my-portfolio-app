import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public about: string;
  public portfolio: string;
  public services: string;
  public contact: string;

 
  constructor() {

    this.about = 'ABOUT';
    this.portfolio = 'PORTFOLIO';
    this.services = 'SERVICES';
    this.contact = 'CONTACT';


   }

  ngOnInit(): void {
  }

  toIntro() {
    document.getElementById("intro").scrollIntoView();
  }

  toAbout() {
    document.getElementById("about").scrollIntoView({behavior:'smooth'});
  }

  toPortfolio() {
    document.getElementById("portfolio").scrollIntoView({behavior:'smooth'});
  }

  toServices() {
    document.getElementById("services").scrollIntoView({behavior:'smooth'});
  }

  toContact() {
    document.getElementById("contact").scrollIntoView({behavior:'smooth'});
  }

  
 
  
}
